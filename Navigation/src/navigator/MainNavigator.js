import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons'

//Screen to navigate
import HomeScreen from '../screens/HomeScreen';
import SearchScreen from '../screens/SearchScreen'
import CreatePostScreen from '../screens/CreatePostScreen';
import ProfileStack from './ProfileStack'
import HomeStack from './HomeStack'

//Bottom Tab Navigator
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

const Tab = createBottomTabNavigator();

function MainNavigator(){
    return (
        <Tab.Navigator
            screenOptions = {({route}) => ({
                tabBarIcon: ({color, size}) => {
                    let iconName;

                    if (route.name === 'Home'){
                        iconName = 'home-outline'
                    } else if (route.name === 'Search'){
                        iconName = 'search-outline'
                    } else if (route.name === 'Post'){
                        iconName = 'add-circle-outline'
                    } else if (route.name === 'Profile'){
                        iconName = 'person-circle-outline'
                    }
                    return <Ionicons name={iconName} size={size} color={color}/>
                }
            })}
            tabBarOptions={{
                activeTintColor: '#FF65C5',
                inactiveTintColor: '#808080'
            }}
            >
            <Tab.Screen name='Home' component={HomeStack}/>
            <Tab.Screen name='Search' component={SearchScreen}/>
            <Tab.Screen name='Post' component={CreatePostScreen}/>
            <Tab.Screen name='Profile' component={ProfileStack}/>
        </Tab.Navigator>
    )
}

export default MainNavigator;