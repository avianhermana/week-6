import React from 'react';

//Navigator
import {createStackNavigator} from '@react-navigation/stack';
const Stack = createStackNavigator();

//Screens
import HomeScreen from '../screens/HomeScreen'
import RequestScreen from '../screens/RequestScreen'
import PetDetails from '../screens/DetailPost'

export default function HomeStack(){
    return (
        <Stack.Navigator headerMode='none'>
            <Stack.Screen name='Home' component={HomeScreen}/>
            <Stack.Screen name='Request' component={RequestScreen}/>
            <Stack.Screen name='PetDetail' component={PetDetails}/>
        </Stack.Navigator>
    )
}