import React from 'react';

//Navigator
import {createStackNavigator} from '@react-navigation/stack';
const Stack = createStackNavigator();

//Screens
import ProfileScreen from '../screens/ProfileScreen'
import EditProfileScreen from '../screens/EditProfileScreen'
import DetailNotification from '../screens/DetailNotification'
import DetailPost from '../screens/DetailPost'
import PeopleProfileScreen from '../screens/PeopleProfileScreen'

export default function ProfileStack(){
    return (
        <Stack.Navigator headerMode='none'>
            <Stack.Screen name='Profile' component={ProfileScreen}/>
            <Stack.Screen name='Edit Profile' component={EditProfileScreen}/>
            <Stack.Screen name='Detail Notification' component={DetailNotification}/>
            <Stack.Screen name='Detail Post' component={DetailPost}/>
            <Stack.Screen name='People Profile Screen' component={PeopleProfileScreen}/>
        </Stack.Navigator>
    )
}