export const EDIT_PROFILE = 'EDIT_PROFILE';
export const EDIT_PROFILE_SUCCESS = 'EDIT_PROFILE_SUCCESS';
export const EDIT_PROFILE_FAILED = 'EDIT_PROFILE_FAILED'; 

export const getProfileDetail = () => {
  return {type: 'GET_PROFILE'};
};

export const editProfileDetail = (payload) => {
  return {
    type: 'EDIT_PROFILE',
    payload,
  };
};
