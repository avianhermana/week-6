import axios from 'axios';
import {takeLatest, put} from 'redux-saga/effects';
import {getAccountId, getHeaders} from '../../common/function/auth';
import {getPostApi} from '../../common/api/auth';

function* getPost(){
    
    try {
    const headers = yield getHeaders();
    // console.log('ini header>>',headers)
    const accountId = yield getAccountId();
    // console.log('ini accountID>>', accountId);
    const resPost = yield getPostApi(accountId, headers);
    yield put({type: 'GET_POST_SUCCESS', data: resPost.data})
    // console.log('respost>>',resPost)
    }
    catch (err){
        console.log(err)
    }
    yield put({type: 'GET_POST_FAILED'})
}

function* postSaga(){
    yield takeLatest('GET_POST', getPost)
}

export default postSaga