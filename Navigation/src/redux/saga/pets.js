import axios from 'axios';
import {takeLatest, put} from 'redux-saga/effects';

function* getPets(){
    try {
        const resPets = yield axios({
            method: 'GET',
            url: 'https://api.tinpet.my.id/pet'
        })

        if (resPets && resPets.data){
            console.log(resPets.data.length)
            yield put({type: 'GET_PETS_SUCCESS', data: resPets.data})
        }
    }
    catch {
        console.log(err)
    }
}

function* petSaga(){
    yield takeLatest('GET_PETS', getPets)
}

export default petSaga