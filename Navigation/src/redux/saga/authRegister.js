import {put, takeLatest} from 'redux-saga/effects';
import axios from 'axios'

function* register(action){
    try{
        //send data login
        const resRegister = yield axios({
            method: 'POST',
            url: 'https://api.tinpet.my.id/auth/register',
            data: action.payload
        })
        console.log(resRegister.data)
        yield put({type: 'REGISTER_SUCCESS'})
        // yield put({type: 'LOGIN_SUCCESS'})
    }
    catch (err) {
        console.log(err)
        yield put({type: 'REGISTER_FAILED'})
    }
}


function* authRegisterSaga() {
    yield takeLatest('REGISTER', register)
}

export default authRegisterSaga;