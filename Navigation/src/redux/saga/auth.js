import {put, takeLatest} from 'redux-saga/effects';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {saveToken, saveAccountId} from '../../common/function/auth';


function* login(action){
    try{
        //send data login
        const resLogin = yield axios({
            method: 'POST',
            url: 'https://api.tinpet.my.id/auth/login',
            data: action.payload
        })

        if (resLogin && resLogin.data) {
            //simpan token
            yield saveToken(resLogin.data.token);
            yield saveAccountId(resLogin.data.id);
            
            yield put({type: 'LOGIN_SUCCESS'})
            yield put({type: 'GET_PETS'})
            yield put({type: 'GET_POST'})
            yield put({type: 'GET_PROFILE'})
            console.log("ini console log auth saga>>",resLogin.data.id);
            }
        else {
            // show alert
            ToastAndroid.showWithGravity(
            'Login gagal',
            ToastAndroid.SHORT,
            ToastAndroid.BOTTOM,
            );
            yield put({type: LOGIN_FAILED});
        }

        
    }
    catch (err) {
        console.log(err)
        yield put({type: 'LOGIN_FAILED'})
    }
}

function* logout(action){
    try{
        yield AsyncStorage.removeItem('TOKEN');
        yield AsyncStorage.removeItem('ID');
    }
    catch (err) {
        console.log(err)
    }
}

function* authLoginSaga() {
    yield takeLatest('LOGIN', login)
    yield takeLatest('LOGOUT', logout)
}

export default authLoginSaga;