import axios from 'axios';
import {takeLatest, put} from 'redux-saga/effects';
import {getAccountId, getHeaders, getChanso} from '../../common/function/auth';
import {getProfile, apiEditProfile} from '../../common/api/auth';

function* getProfileData(){
    
    try {
    const headers = yield getHeaders();
    // console.log('header saga>>',headers)
    const accountId = yield getAccountId();
    // console.log('profileSaga accountID>>', accountId);
    const resGetProfile = yield getProfile(headers);
    yield put({type: 'GET_PROFILE_SUCCESS', data: resGetProfile.data})
    // console.log('respost>>',resGetProfile)
    }
    catch (err){
        console.log(err)
    }
    yield put({type: 'GET_PROFILE_FAILED'})
}

function* editProfile(action){
    
    try {
    const headers = yield getChanso();
    console.log('header saga>>',headers)
    
    const resEditProfile = yield apiEditProfile(headers, action.payload);
    
    yield put({type: 'EDIT_PROFILE_SUCCESS', data: resEditProfile.data})
    console.log('berhasilll>>',resEditProfile.data)
    yield put({type: 'GET_PROFILE'})
    }
    catch (err){
        console.error("editProfile>>>>", JSON.stringify(err))
    }
    yield put({type: 'EDIT_PROFILE_FAILED'})
}

function* profileSaga(){
    yield takeLatest('GET_PROFILE', getProfileData)
    yield takeLatest('EDIT_PROFILE', editProfile)
}

export default profileSaga