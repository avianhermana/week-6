import {all} from 'redux-saga/effects'
import authLoginSaga from './auth'
import authRegisterSaga from './authRegister'
import petSaga from './pets'
import postSaga from './postSaga'
import profileSaga from './profileSaga'

export default function* rootSaga(){
    yield all([authLoginSaga(), authRegisterSaga(), petSaga(), postSaga(), profileSaga() ])
}