const initialState = {
    petData: [],
    isLoading: false
}

const petList = (state = initialState, action) => {
    switch (action.type){
        case 'GET_PETS':{
            return {
                ...state,
                isLoading: true
            }
        }
        case 'GET_PETS_SUCCESS':{
            return {
                petData: action.data,
                isLoading: false
            }
        }
        default:
            return state;
    }
}

export default petList;