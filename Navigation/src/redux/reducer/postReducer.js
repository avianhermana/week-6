const initialState = {
    postData: [],
    isLoading: false
}

const postList = (state = initialState, action) => {
    switch (action.type){
        case 'GET_POST':{
            return {
                ...state,
                isLoading: true
            }
        }
        case 'GET_POST_SUCCESS':{
            return {
                ...state,
                postData: action.data,
                isLoading: false
            }
        }
        default:
            return {...state, isLoading:false}
    }
}

export default postList;