import {combineReducers} from 'redux'
import login from './auth';
import register from './authRegister'
import petList from './pets'
import postList from './postReducer'
import profileList from './profileReducer'

export default combineReducers({
    login, register, petList, postList, profileList,
})