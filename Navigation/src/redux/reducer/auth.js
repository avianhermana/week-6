const initialState = {
    isLoading: false,
    isLoggedIn: false
}

const auth = (state = initialState, action) => {
    switch (action.type){
        case 'LOGIN':{
            return {
                ...state,
                isLoading: true
            }
        }
        case 'LOGIN_SUCCESS':{
            return {
                isLoggedIn: true,
                isLoading: false
            }
        }
        case 'LOGIN_FAILED': {
            return {
                isLoading: false
            }
        }
        case 'LOGOUT': {
            return {
                isLoggedIn: false,
                isLoading: false
            }
        }
        default:
            return state;
    }
}

export default auth;