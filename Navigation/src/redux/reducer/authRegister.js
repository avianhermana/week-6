const initialState = {
    isLoading: false,
    isRegistered: false
}

const register = (state = initialState, action) => {
    switch (action.type){
        case 'REGISTER':{
            return {
                isLoading: true
            }
        }
        case 'REGISTER_SUCCESS':{
            return {
                isLoading: false,
                isRegistered: true,
            }
        }
        case 'REGISTER_FAILED': {
            return {
                isLoading: false
            }
        }
        default:
            return state;
    }
}

export default register;