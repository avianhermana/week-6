const initialState = {
    profileData: [],
    isLoading: false
}

const profileList = (state = initialState, action) => {
    switch (action.type){
        case 'GET_PROFILE':{
            return {
                ...state,
                isLoading: true
            }
        }
        case 'GET_PROFILE_SUCCESS':{
            return {
                ...state,
                profileData: action.data,
                isLoading: false
            }
        }
        case 'EDIT_PROFILE':{
            return {
                ...state,
                profileData: action.data,
                isLoading: true
            }
        }
        case 'EDIT_PROFILE_SUCCESS':{
            return {
                ...state,
                profileData: action.data,
                isLoading: false
            }
        }
        case 'EDIT_PROFILE_FAILED':{
            return {
                ...state,
                isLoading: false
            }
        }
        default:
            return {...state, isLoading:false}
    }
}

export default profileList;