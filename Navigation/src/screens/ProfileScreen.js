import React, {useEffect, useState} from 'react';
import {View, Text, Image, StyleSheet, StatusBar, Dimensions, TextInput, Modal, TouchableHighlight, TouchableOpacity, ScrollView, ActivityIndicator} from 'react-native';
// import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {TabView, TabBar, SceneMap} from 'react-native-tab-view';
import axios from 'axios';
import EditProfileScreen from './EditProfileScreen';
import DetailNotification from './DetailNotification';
import DetailPost from './DetailPost';
import Post from './Post.Component';
import RequestMeeting from './RequestMeeting.Component';
import { connect } from 'react-redux';

const dw = Dimensions.get('screen').width*(1); 
const dh = Dimensions.get('screen').height; 

const FirstRoute = (props) => (
    // <Post/>
    <View style={{marginTop:20}}><Post navigation={props.navigation}/></View>
  );
  
  const SecondRoute = () => (
      // <RequestMeeting/>
    <View style={{marginTop:20}}><RequestMeeting/></View>
  );
  

function ProfileScreen(props){
    
    const [image, setImage] = useState();
    
    // useEffect(() => {
    //     // props.profile.userImage.url
    //     setImage(props.profile.userImage.url)
    // }, [props.profile]);
    // console.log(props.profile)

    const [modalVisible, setModalVisible] = useState(false);
    const initialLayout = { width:200, height:600 };
    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: 'first', title: 'Post' },
        { key: 'second', title: 'Request Meeting' },
    ]);

    const renderScene = SceneMap({
        first: FirstRoute,
        second: SecondRoute,
    });
    const renderTabBar = props => (
        <TabBar
          {...props}
          indicatorStyle={{ backgroundColor: "#FF65C5" }}
          style={{ backgroundColor: "white" }}
          activeColor="#FF65C5"
          inactiveColor="#FF65C5"
        />
      );

    //   console.log("props.profile>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ",props.profile.userImage?props.profile.userImage.url:null);

    return(
            <View style={styles.container}>
                {/*HEADER*/}
                <View style={{backgroundColor:'white', width:dw, height:50,marginBottom:15, flexDirection:'row'}}>
                    <Text style={{fontSize:24, color:'#000000', fontWeight:"bold", marginLeft:dw/(2.3), marginTop:10, marginBottom:10, marginRight:90}}>Profile</Text>
                    
                    {/*NOTIFICATION*/}
                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={modalVisible}
                        onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                        }} >
                        <View>
                        <View style={styles.modalView}>
                            <TouchableHighlight
                            style={{ ...styles.closeButton, backgroundColor: "pink" }}
                            onPress={() => {
                                setModalVisible(!modalVisible);
                            }}
                            >
                                <Text style={styles.textStyle}>X</Text>
                            </TouchableHighlight>

                            <View>
                                <TouchableOpacity style={styles.containNotif} onPress={() => {
                                    setModalVisible(false);
                                    props.navigation.navigate('Detail Notification');
                                }}>
                                    <Image style={styles.photoNotif} source={require('../assets/wongchileek.png')}/>
                                    <Text style={styles.modalTextName}>Wong Chi Liek</Text>
                                    <Text style={styles.modalTextNotif}> Like your post</Text>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <TouchableOpacity style={styles.containNotif} onPress={() => {
                                    setModalVisible(false);
                                    props.navigation.navigate('Detail Notification');
                                }}>
                                    <Image style={styles.photoNotif} source={require('../assets/wongchileek.png')}/>
                                    <Text style={styles.modalTextName}>Wong Chi Liek</Text>
                                    <Text style={styles.modalTextNotif}> Comment on your post</Text>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <TouchableOpacity style={styles.containNotif} onPress={() => {
                                    setModalVisible(false);
                                    props.navigation.navigate('Detail Notification');
                                }}>
                                <Image style={styles.photoNotif} source={require('../assets/wongchileek.png')}/>
                                <Text style={styles.modalTextName}>Wong Chi Liek</Text>
                                <Text style={styles.modalTextNotif}> Send you request</Text>
                                </TouchableOpacity>
                            </View>

                        </View>
                        </View>
                    </Modal>

                    <TouchableHighlight onPress={() => {setModalVisible(true)}}>
                        <Ionicons style={{ marginTop:10, marginLeft: -15}} name="notifications" size={27} color='grey'/>
                    </TouchableHighlight>
                    <Ionicons style={{paddingLeft:10, marginTop:10}} name="exit" size={27} color='grey'/>
                </View>
                <ScrollView>
                <View>
                    <View style={styles.cardProfile} >
                        {/* {loading == null ? ( */}
                            {/* <ActivityIndicator size="large" color="#222"/> */}
                        {/* ):( */}
                            <Image style={styles.logoAvatar} source={{uri:props.profile.userImage.url}}/>
                            {/* )} */}
                        <View style={styles.profileName}>
                            {/* <Text style={{fontWeight:'bold', fontSize:18}}>{props.profile.name}</Text> */}
                        </View>
                        <View>
                            <Text style={styles.profileDescription}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc tincidunt.</Text>
                        </View>
                        <View style={{alignItems:"center"}}>
                            <TouchableOpacity onPress={() => props.navigation.navigate('Edit Profile')}>
                                <Text style={styles.editProfile}>Edit Profile</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                
                {/* POST & REQUEST MEETING */}
                <View style={styles.box}>
                    <TabView
                    navigationState={{ index, routes }}
                    renderScene={renderScene}
                    navigation={props.navigation}
                    onIndexChange={setIndex}
                    initialLayout={initialLayout}
                    renderTabBar={renderTabBar}
                    />
                </View>
                </ScrollView>
            </View>
        
    );
}

const mapStateToProps = (state) => ({
    profile : state.profileList.profileData,
    loading : state.profileList.isLoading
    })

const mapDispatchToProps = (dispatch) => ({
    getProfileData : () => dispatch({type:'GET_PROFILE'})
})

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen)

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: "center",
        alignItems: 'center',
        marginTop:10,
    },
    //CSS MODAL
    modalView: {
        margin: 10,
        marginTop:60,
        backgroundColor: "white",
        borderTopLeftRadius:10,
        borderBottomLeftRadius:10,
        borderBottomRightRadius:10,
        paddingBottom:10,
        shadowOffset: {
        width: 0,
        height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 19
    },
    closeButton: {
        width:25,
        height:25,
        borderRadius: 50,
        marginLeft: 375,
        marginTop:10,
        marginBottom:10,
        elevation: 2,
        textAlign:"center"
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center",
        marginTop:3,
    },
    modalTextName: {
        marginBottom: 15,
        textAlign: "center",
        marginTop:15,
        marginLeft:30,
        fontWeight:"bold"
    },
    modalTextNotif: {
        marginBottom: 15,
        textAlign: "center",
        marginTop:15,
        marginLeft:5,
    },
    logoAvatar:{
        maxWidth: 100,
        maxHeight: 100,
        alignContent:"center",
        marginLeft:'39%',
        marginTop:'10%',
        marginBottom:2,
        backgroundColor:'white',
        borderRadius:50,
        width:100,
        height:100
    },
    cardProfile:{
        backgroundColor:'white',
        borderColor:'grey',
        borderRadius:25,
        width:400,
        height:300,
        marginTop:1,
        alignContent:"center"
    },
    cardPost:{
        backgroundColor:'white',
        borderRadius:10,
        width:400,
        height:470,
        marginTop:1,
        marginBottom:1,
        alignContent:"center"
    },
    cardPostImage:{
        borderRadius:24,
        width:400,
        marginTop:1,
        marginBottom:1,
        alignContent:"center"
    },
    box:{
        position:'relative',
        flexDirection:'row',
        borderColor:'black',
        width:'100%',
        height:'100%',
        marginTop:30,
        alignContent:"center",
        marginBottom:20,
        marginTop:10,
    },
    post:{
        width:200,
        marginTop:'8%',
        color:'#FF65C5',
        textAlign:"center",
        fontSize:18,
        fontWeight:"bold"
    },
    requestMeeting:{
        width:200,
        marginTop:'8%',
        textAlign:"center",
        fontSize:18,
        color:'#C4C4C4'
    },
    editProfile:{
        width:200,
        marginTop:10,
        color:'#FF65C5',
        textAlign:"center",
        fontSize:18,
        fontWeight:"bold"
    },
    profileName:{
        fontWeight:"bold",
        alignItems:"center",
        fontSize:24,
        marginBottom:10,
        marginTop:10,
    },
    cardProfileName:{
        fontWeight:"bold",
        alignItems:"center",
        fontSize:24,
        marginBottom:5,
        flexDirection:'row'
    },
    profileDescription:{
        width:250,
        textAlign:"center",
        marginLeft:'20%'
    },
    containNotif:{
        flexDirection:'row',
        marginLeft:30,
        marginBottom:10,
        width:340,
        borderBottomWidth:1,
        borderBottomColor:'lightgrey'
    },
    photoNotif:{
        width:50,
        height:50,
        borderRadius:50,
        marginBottom:10
    }
})
