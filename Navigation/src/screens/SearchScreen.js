import React, {useState} from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Ionicons from 'react-native-vector-icons/Ionicons'
import {Picker} from '@react-native-picker/picker'
import { connect } from 'react-redux';
import PetCard from '../components/PetCard.component'

function SearchScreen(props){
    const [city, setCity] = useState();
    const [category, setCategory] = useState();

    return(
        <View>
            <View style={styles.headerContainer}>
                <Image 
                source={require('../assets/tinpet-logo.png')}
                style={styles.logo}/>
                <Ionicons name='notifications' size={wp('6.5%')} color='#9C9C9C' />
            </View>
            <View style={{backgroundColor: '#FFFFFF', borderRadius: 10, marginHorizontal: hp('3.5%'), marginVertical: hp('2.5%')}}>
                <View style={{flexDirection: 'column', padding: 10, justifyContent: 'center', alignItems: 'center'}}>
                    <View>
                        <Text style={styles.widgetTitle}>Location</Text>
                        <View style={{borderWidth: 1, borderColor: '#C4C4C4', backgroundColor: '#EDEDED', borderRadius: 5, width: wp('70%'), marginVertical: 5}}>
                            <Picker
                            selectedValue={city}
                            style={{height: hp('6%'), width: wp('70%')}}
                            mode="dropdown"
                            onValueChange={(itemValue, itemIndex) => setCity(itemValue)}>
                                <Picker.Item label="Jakarta" value="Jakarta" />
                                <Picker.Item label="Bogor" value="Bogor" />
                                <Picker.Item label="Depok" value="Depok" />
                                <Picker.Item label="Tangerang" value="Tangerang" />
                                <Picker.Item label="Bekasi" value="Bekasi" />
                                <Picker.Item label="Bandung" value="Bandung" />
                            </Picker>
                        </View>
                    </View>
                    <View>
                        <Text style={styles.widgetTitle}>Category</Text>
                        <View style={{borderWidth: 1, borderColor: '#C4C4C4', backgroundColor: '#EDEDED', borderRadius: 5, width: wp('70%'), marginVertical: 5}}>
                            <Picker
                            selectedValue={category}
                            style={{height: hp('6%'), width: wp('70%')}}
                            mode="dropdown"
                            onValueChange={(itemValue, itemIndex) =>
                            setCategory(itemValue)
                            }>
                                <Picker.Item label="Dog" value="dog" />
                                <Picker.Item label="Cat" value="cat" />
                            </Picker>
                        </View>
                    </View>
                </View>
                <TouchableOpacity style={styles.searchBtn}>
                    <Text style={styles.textBtn}>Search</Text>
                </TouchableOpacity>
            </View>
            <View>
                <Text style={{alignSelf: 'center', fontSize: 18, fontWeight: 'bold', color: '#FF65C5'}}>Found 10 results</Text>
            </View>
            <PetCard/>
        </View>
    )
}

const styles = StyleSheet.create({
    headerContainer: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        height: hp('7.5%'), 
        borderColor: '#E5E5E5', 
        borderWidth: 0.9,
        backgroundColor: '#FFFFFF',
        paddingHorizontal: 15
    },
    logo: {
        marginLeft: wp('31%'),
        width: wp('27%'),
        height: hp('3.7%'),
    },
    container: {
        flex: 1,
        width: '90%',
        height: 278,
        backgroundColor: '#ffff',
        borderRadius: 15,
        marginLeft: '5%',
        marginRight: '5%',
        borderColor: '#E5E5E5',
        borderWidth: 1,
    },
    input: {
        width: '85%',
        height: 50,
        backgroundColor: '#F4F4F4',
        borderRadius: 5,
        borderColor: '#C4C4C4',
        borderWidth: 1,
        fontSize: 18,
        marginTop: 32.17,
        marginHorizontal: '7.5%',
    },
    inputText: {
        marginLeft: '5%',
    },
    input1: {
        width: '85%',
        height: 50,
        backgroundColor: '#F4F4F4',
        borderRadius: 5,
        borderColor: '#C4C4C4',
        borderWidth: 1,
        fontSize: 18,
        marginTop: 19,
        marginHorizontal: '7.5%',
    },
    searchBtn: {
        width: '85%',
        height: 55,
        backgroundColor: '#3E4C6F',
        borderRadius: 5,
        marginTop: 10,
        marginBottom: 20,
        marginHorizontal: '7.5%',
    },
    textBtn: {
        fontFamily: 'montserrat',
        fontWeight: 'bold',
        fontSize: 18,
        color: '#fff',
        height: 22,
        width: 130,
        marginTop: 14,
        marginHorizontal: '41%',
    },
    widgetTitle: {
        fontSize: 16, 
        fontWeight: 'bold',
        marginTop: 10
    }
    
})

const reduxState = (state) => ({
    
})

const reduxDispatch = () => ({

})

export default connect(reduxState, reduxDispatch)(SearchScreen)