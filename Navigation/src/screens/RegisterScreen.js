import React, {useState} from 'react'
import {connect} from 'react-redux'
import {TextInput, View, Text, Image, TouchableOpacity, StyleSheet, ImageBackground, ScrollView} from 'react-native'

const RegisterScreen = (props) => {
    const [email, setEmail] = useState();
    const [fullName, setFullName] = useState();
    const [password, setPassword] = useState();
    const [confirm, setConfirm] = useState();

    const handleRegister = () => {
        if (!email) {
            alert('Email is required')
        }
        else if (!password) {
            alert('Password is required')
        }
        else if (!fullName) {
            alert('Name is required')
        }
        else if (!confirm) {
            alert('Re-enter your password')
        }
        else if (confirm !== password) {
            alert("Password doesn't match")
        }
        else {
            props.register({
                name: fullName,
                email,
                password,
            })
        }
    }

    const SignUpButton = () => {
        return (
            <TouchableOpacity
            style={styles.buttonWrapper}
            onPress={() => handleRegister()}>
                <Text style={styles.loginTextLabel}>Sign Up</Text>
            </TouchableOpacity>
        )
    }

    const LoadingButton = () => {
        return (
            <TouchableOpacity
            style={styles.buttonWrapperLoading}>
                <Text style={styles.loginTextLabel}>Loading...</Text>
            </TouchableOpacity>
        )
    }

    return(
        <ScrollView contentContainerStyle={{minHeight: '100%'}}>
            <ImageBackground
            source={require('../assets/login-background.png')}
            style={styles.backgroundImage}>
                <View style={styles.floatContainer}>
                    <Image 
                    source={require('../assets/tinpet-logo.png')}
                    style={styles.logo}/>
                    <View style={{padding: 22}}>
                        <Text style={styles.textLabel}>Full Name</Text>
                        <TextInput 
                        placeholder='Input your full name'
                        placeholderTextColor='#C4C4C4'
                        value={fullName}
                        style={styles.input}
                        onChangeText={(fullName) => setFullName(fullName)}
                        />
                        <Text style={styles.textLabel}>Email</Text>
                        <TextInput 
                        placeholder='Input your email'
                        placeholderTextColor='#C4C4C4'
                        value={email}
                        style={styles.input}
                        onChangeText={(email) => setEmail(email)}
                        />
                        <Text style={styles.textLabel}>Password</Text>
                        <TextInput 
                        placeholder='Input your password'
                        placeholderTextColor='#C4C4C4'
                        value={password}
                        style={styles.input}
                        onChangeText={(password) => setPassword(password)}
                        secureTextEntry
                        />
                        <Text style={styles.textLabel}>Confirm Password</Text>
                        <TextInput 
                        placeholder='Re-input your password'
                        placeholderTextColor='#C4C4C4'
                        value={confirm}
                        style={styles.input}
                        onChangeText={(confirm) => setConfirm(confirm)}
                        secureTextEntry
                        />
                        
                        { props.loading ? <LoadingButton /> : <SignUpButton/>}
                    </View>
                </View>
        </ImageBackground>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    backgroundImage: {
        resizeMode: 'cover',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
    },
    floatContainer: {
        width: 343,
        height: 575,
        margin: 16,
        marginTop: -5,
        borderColor: '#C4C4C4',
        borderWidth: 1,
        borderRadius: 15,
        backgroundColor: '#ffffff'
    },
    logo: {
        width: 120,
        height: 30,
        alignSelf: 'center',
        marginTop: 39,
        marginBottom: 5
    },
    input: {
        fontSize: 16,
        paddingVertical: 5,
        marginBottom: 10,
        backgroundColor: '#f5f5f5',
        borderRadius: 5,
        borderColor: '#C4C4C4',
        borderWidth: 1,
        paddingLeft: 10,
        paddingVertical: 10,
        color: '#C4C4C4'
    },
    textLabel: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 8,
    },
    loginTextLabel: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#ffffff'
    },
    buttonWrapper: {
        backgroundColor: '#FF65C5',
        alignItems: 'center',
        padding: 16,
        borderRadius: 5,
        marginTop: 20
    },
    buttonWrapperLoading: {
        backgroundColor: '#dc43a6',
        alignItems: 'center',
        padding: 16,
        borderRadius: 5,
        marginTop: 20
    },
})

const reduxState = (state) => ({
    loading: state.register.isLoading
})

const reduxDispatch = (dispatch) => ({
    register: (data) => dispatch({type: 'REGISTER', payload: data})
})

export default connect(reduxState, reduxDispatch)(RegisterScreen);
