import React, {useState} from 'react';
import {View, Text, TextInput, TouchableOpacity, StyleSheet} from 'react-native'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Ionicons from 'react-native-vector-icons/Ionicons'
import DateTimePicker from '@react-native-community/datetimepicker';
import {Picker} from '@react-native-picker/picker'

const RequestScreen = (props) => {
    const [date, setDate] = useState();
    const [time, setTime] = useState();
    const [isShowDate, setIsShowDate] = useState(false);
    const [isShowTime, setIsShowTime] = useState(false);
    const [message, setMessage] = useState();
    const [location, setLocation] = useState();

    const convertDateToString = (selectedDate) => {
        const currDate = ('0' + selectedDate.getDate()).slice(-2);
        const currMonth = selectedDate.getMonth() + 1;
        const currYear = selectedDate.getFullYear();
        console.log(currDate + '-' + currMonth + '-' + currYear)
        return currDate + '-' + currMonth + '-' + currYear
    }
    const convertTimeToString = (selectedTime) => {
        const currHour = ('0' + selectedTime.getHours()).slice(-2);
        const currMinute = ('0' + selectedTime.getMinutes()).slice(-2);
        console.log(currHour + ':' + currMinute)
        return currHour + ':' + currMinute
    }

    const onChangeDate = (event, selectedDate) => {
        setIsShowDate(false)
        const currentDate = selectedDate || date;
        setDate(currentDate);
    };
    const onChangeTime = (event, selectedTime) => {
        setIsShowTime(false)
        const currentTime = selectedTime || time;
        setTime(currentTime);
    };
    
    return (
        <View style={styles.container}>
            <View style={styles.headerContainer}>
                <Ionicons name='arrow-back' size={25} />
                <Text style={styles.headerTitle}>Request Meeting</Text>
                <Ionicons name='notifications' size={25} color='#9C9C9C' />
            </View>
            <View style={styles.widgetContainer}>
                <View style={styles.dateTimeContainer}>
                    <View style={styles.dateTimeSeparator}>
                        <Text style={styles.widgetTitle}>Date</Text>
                        <TouchableOpacity
                        onPress={() => setIsShowDate(true)}
                        style={styles.dateTimeInput}>
                            <Text style={styles.dateTimeText}>{date? convertDateToString(date) : 'select date'}</Text>
                        </TouchableOpacity>
                        {isShowDate && (
                            <DateTimePicker
                            testID="dateTimePicker"
                            value={date? date : new Date()}
                            mode={'date'}
                            is24Hour={true}
                            display="default"
                            onChange={onChangeDate}
                            />
                        )}
                    </View>
                    <View style={styles.dateTimeSeparator}>
                        <Text style={styles.widgetTitle}>Hour</Text>
                        <TouchableOpacity
                            onPress={() => setIsShowTime(true)}
                            style={styles.dateTimeInput}>
                            <Text style={styles.dateTimeText}>{time? convertTimeToString(time) : 'select time'}</Text>
                        </TouchableOpacity>
                        {isShowTime && (
                            <DateTimePicker
                            testID="dateTimePicker"
                            value={time? time : new Date()}
                            mode={'time'}
                            is24Hour={true}
                            display="default"
                            onChange={onChangeTime}
                            />
                        )}
                    </View>
                </View>
                <View style={styles.messageContainer}>
                    <Text style={styles.widgetTitle}>Leave a message</Text>
                    <TextInput
                    placeholder='Write your message here'
                    placeholderTextColor='#808080'
                    value={message}
                    multiline={true}
                    numberOfLines={4}
                    textAlignVertical='top'
                    style={styles.messageInput}
                    onChangeText={(message) => setMessage(message)}
                    />
                </View>
                <View style={styles.locationContainer}>
                    <Text style={styles.widgetTitle}>Location</Text>
                    <View style={styles.locationInput}>
                        <Picker
                        selectedValue={location}
                        mode='dropdown'
                        onValueChange={(itemValue, itemIndex) => setLocation(itemValue)}
                        >
                            <Picker.Item label='Jakarta' value='Jakarta'/>
                            <Picker.Item label='Bogor' value='Bogor'/>
                            <Picker.Item label='Depok' value='Depok'/>
                            <Picker.Item label='Tangerang' value='Tangerang'/>
                            <Picker.Item label='Bekasi' value='Bekasi'/>
                            <Picker.Item label='Bandung' value='Bandung'/>
                        </Picker>
                    </View>
                </View>

                <View style={{flexDirection: 'row', marginHorizontal: 20, marginTop: hp('27%'), justifyContent: 'center'}}>
                    <TouchableOpacity
                    onPress={() => props.navigation.navigate('Home')}
                    style={styles.cancelButton}>
                        <Text style={styles.cancelText}>Cancel</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                    onPress={() => props.navigation.navigate('Home')}
                    style={styles.postButton}>
                        <Text style={styles.postText}>Post</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
    
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F0F0F0',
    },
    headerContainer: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        height: hp('7.5%'), 
        borderColor: '#E5E5E5', 
        borderWidth: 0.9,
        backgroundColor: '#FFFFFF',
        paddingHorizontal: 15
    },
    headerTitle: {
        fontSize: hp('2.5%'), 
        fontWeight: 'bold'
    },
    widgetContainer: {
        marginTop: hp('2.5%'), 
        backgroundColor: '#FFFFFF', 
        borderTopLeftRadius: 20, 
        borderTopRightRadius: 20
    },
    dateTimeContainer: {
        flexDirection: 'row', 
        justifyContent: 'center',
    },
    dateTimeSeparator: {
        margin: 20
    },
    widgetTitle: {
        marginBottom: 10, 
        fontSize: 16, 
        fontWeight: 'bold'
    },
    dateTimeInput: {
        width: 160, 
        backgroundColor: '#EDEDED', 
        borderColor: '#C4C4C4', 
        borderWidth: 1, 
        borderRadius: 5, 
        padding: 10, 
        alignItems: 'center'
    },
    dateTimeText: {
        color: '#808080'
    },
    messageContainer: {
        marginHorizontal: 20, 
        marginBottom: 20
    },
    locationContainer: {
        marginHorizontal: 20
    },
    messageInput: {
        backgroundColor: '#EDEDED',
        borderColor: '#C4C4C4', 
        borderWidth: 1,
        borderRadius: 5, 
        padding: 10
    },
    locationInput: {
        height: 50, 
        width: 150, 
        borderColor: '#C4C4C4', 
        borderWidth: 1, 
        borderRadius: 5, 
        backgroundColor: '#EDEDED'
    },
    cancelButton: {
        height: 50, 
        width: 150, 
        borderColor: '#000000', 
        borderRadius: 5, 
        borderWidth: 1, 
        marginHorizontal: 20, 
        justifyContent: 'center', 
        alignItems: 'center',
        marginBottom: hp('10%')
    },
    postButton: {
        height: 50, 
        width: 150, 
        borderColor: '#FF65C5', 
        borderRadius: 5, 
        borderWidth: 1, 
        marginHorizontal: 20, 
        justifyContent: 'center', 
        alignItems: 'center', 
        backgroundColor: '#FF65C5',
    },
    cancelText: {
        fontWeight: 'bold', 
        fontSize: hp('2.4%')
    },
    postText: {
        color: '#FFFFFF', 
        fontWeight: 'bold', 
        fontSize: hp('2.4%')
    }
})



export default RequestScreen;