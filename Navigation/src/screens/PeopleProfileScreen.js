import React, {useState} from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity, ScrollView} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';


// const consWidth = Dimensions.get('window').width * 0.8;
// const consHeigth = Dimensions.get('window').height;


export default function PeopleProfileScreen(props){
    const [modalVisible, setModalVisible] = useState(false);
    
    return(
            <View style={styles.container}>
                {/*HEADER*/}
                
                <ScrollView>
                <View>
                    <View style={styles.cardProfile} >
                        <Image style={styles.logoAvatar} source={require("../assets/wongchileek.png")}></Image>
                        <View style={styles.profileName}>
                            <Text style={{fontWeight:'bold'}}>Wong Chi Liek</Text>
                        </View>
                        <View>
                            <Text style={styles.profileDescription}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc tincidunt.</Text>
                        </View>
                    </View>
                </View>
                <View>
                    <View style={styles.box}>
                        <TouchableOpacity><Text style={styles.post}>P o s t</Text></TouchableOpacity>
                    </View>
                </View>
                <View>
                    <View style={styles.cardPost}>
                        <View>
                            <View style={styles.cardProfileName}>
                            <Image style={{width:40,height:40, borderRadius:50, position:"relative", marginTop:10, marginLeft:10, borderColor:'white' }} source={require("../assets/wongchileek.png")}></Image>
                                <Text style={{fontWeight:'bold', marginTop: 10, marginLeft:15}}>Wong Chi Liek</Text>
                            </View>
                        </View>
                        <TouchableOpacity onPress={() => props.navigation.navigate("Detail People Post")}>
                            <View>
                                <Image style={styles.cardPostImage} source={require("../assets/kucing.jpeg")}></Image>
                                <View style={{flexDirection:'row', marginTop:5, marginBottom:5, marginLeft:15}}>
                                    <Text>1432 Likes</Text>
                                    <Text style={{marginLeft:10}}>21 Comments</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <View style={{flexDirection:'row', marginLeft:15}}>
                            <Ionicons name="heart-outline" size={30} color="black"/>
                            <Ionicons style={{marginLeft:40}} name="chatbubble-outline" size={30} color="black"/>
                        </View>
                        <TouchableOpacity onPress={() => props.navigation.navigate("Detail People Post")}>
                            <View style={{alignItems:"center", marginTop:10}}>
                                <Text style={{width:350, height: 50, borderRadius:5, backgroundColor:'#FF65C5', color:'#FFFFFF', fontWeight:'bold', fontSize:24, textAlign:'center', paddingTop:7}}>Request Meeting</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                </ScrollView>
            </View>
        
    );
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: "center",
        alignItems: 'center',
        marginTop:10,
    },
    //CSS MODAL
    modalView: {
        margin: 10,
        marginTop:60,
        backgroundColor: "white",
        borderTopLeftRadius:10,
        borderBottomLeftRadius:10,
        borderBottomRightRadius:10,
        paddingBottom:10,
        shadowOffset: {
        width: 0,
        height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 19
    },
    closeButton: {
        width:25,
        height:25,
        borderRadius: 50,
        marginLeft: 375,
        marginTop:10,
        marginBottom:10,
        elevation: 2,
        textAlign:"center"
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center",
        marginTop:3,
    },
    modalTextName: {
        marginBottom: 15,
        textAlign: "center",
        marginTop:15,
        marginLeft:30,
        fontWeight:"bold"
    },
    modalTextNotif: {
        marginBottom: 15,
        textAlign: "center",
        marginTop:15,
        marginLeft:5,
    },
    logoAvatar:{
        maxWidth: 100,
        maxHeight: 100,
        alignContent:"center",
        marginLeft:'39%',
        marginTop:'10%',
        marginBottom:2,
        backgroundColor:'white',
        borderRadius:50,
    },
    cardProfile:{
        backgroundColor:'white',
        borderColor:'grey',
        borderRadius:25,
        width:400,
        height:250,
        marginTop:1,
        alignContent:"center"
    },
    cardPost:{
        // borderWidth:1,
        backgroundColor:'white',
        borderRadius:10,
        width:400,
        height:525,
        marginTop:1,
        marginBottom:100,
        alignContent:"center"
    },
    cardPostImage:{
        borderRadius:24,
        width:400,
        marginTop:1,
        marginBottom:1,
        alignContent:"center"
    },
    box:{
        top:10,
        flexDirection:'row',
        backgroundColor:'white',
        borderColor:'black',
        width:'100%',
        height:56,
        marginBottom:20,
    },
    post:{
        width:400,
        top:'30%',
        color:'#FF65C5',
        textAlign:"center",
        fontSize:18,
        fontWeight:"bold"
    },
    requestMeeting:{
        width:200,
        marginTop:'8%',
        textAlign:"center",
        fontSize:18,
        color:'#C4C4C4'
    },
    editProfile:{
        width:200,
        marginTop:30,
        color:'#FF65C5',
        textAlign:"center",
        fontSize:18,
        fontWeight:"bold"
    },
    profileName:{
        fontWeight:"bold",
        alignItems:"center",
        fontSize:24,
        marginBottom:10,
        marginTop:10,
    },
    cardProfileName:{
        fontWeight:"bold",
        alignItems:"center",
        fontSize:24,
        marginBottom:5,
        flexDirection:'row'
    },
    profileDescription:{
        width:250,
        textAlign:"center",
        marginLeft:'20%'
    },
    containNotif:{
        flexDirection:'row',
        marginLeft:30,
        marginBottom:10,
        width:340,
        borderBottomWidth:1,
        borderBottomColor:'lightgrey'
    },
    photoNotif:{
        width:50,
        height:50,
        borderRadius:50,
        marginBottom:10
    }
})