import React, { useState } from 'react';
import {View, Text, Image, TextInput, StyleSheet} from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-picker';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { editProfileDetail } from '../redux/action/profileAction';
import {useNavigation} from '@react-navigation/native';
import { add } from 'react-native-reanimated';

const options = {
    title: 'Select Image',
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };
  

function EditProfileScreen(props){

    const navigation = useNavigation();
    const [name, setName] = useState()
    const [email, setEmail] = useState()
    const [mobileNumber, setMobileNumber] = useState()
    const [address, setAddress] = useState()
    const [profilePicture, setProfilePicture] = useState(props.profile.userImage.url)
    const [rawImage, setRawImage] = useState({});

    console.log(profilePicture);

    const submit = () => {
      const data = new FormData()
      
        // uploadImage();
          data.append('name', name),
          data.append('email', email),
          data.append('mobileNumber', mobileNumber),
          data.append('address', address),
          data.append('file', rawImage),
        
          console.log('teadasdsasfasdas>>>>>',data)
        props.editProfileData(data);
      };

    function pickImage() {
        ImagePicker.showImagePicker(options, (response) => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            const source = {
              uri: response.uri,
              type: response.type,
              name: response.fileName,
              data: response.data,
            };
    
            setRawImage(source);
            setProfilePicture(response.uri);
          }
        });
      }

    return(
        <View style={styles.container}>
            <ScrollView>
                <View style={styles.cardBody}>
                    <View style={{flexDirection:'row', marginTop:30}}>
                      {/* {profil ? (
                       <ActivityIndicator size="large" color="#222"/>
                      ) : ( */}
                        <Image style={styles.logoAvatar} source={{uri:props.profile.userImage.url}}></Image>
                      {/* )} */}
                        
                        <TouchableOpacity onPress={() => pickImage() }>
                            <Text style={{color:'#FF65C5', fontSize:22, marginTop:'40%',}}> Change Picture</Text>
                        </TouchableOpacity>
                    </View>

                    <View>
                        <Text style={styles.cardText}>Nama</Text>
                        <TextInput style={styles.textInput} placeholder={props.profile.name} value={name} onChangeText={(text)=> setName(text)}/>
                    </View>

                    <View>
                        <Text style={styles.cardText}>Email</Text>
                        <TextInput style={styles.textInput} placeholder={props.profile.email} value={email} onChangeText={(text)=> setEmail(text)}/>
                    </View>

                    <View>
                        <Text style={styles.cardText}>Mobile Number</Text>
                        <TextInput style={styles.textInput} placeholder={props.profile.mobileNumber} value={mobileNumber} onChangeText={(text)=> setMobileNumber(text)}/>
                    </View>

                    <View>
                        <Text style={styles.cardText}>Full Address</Text>
                        <TextInput style={styles.textInput} placeholder={props.profile.address} value={address} onChangeText={(text)=> setAddress(text)}/>
                    </View>

                    <View style={{flexDirection:'row', paddingTop:50, paddingLeft:30}}>
                        <TouchableOpacity onPress={() => props.navigation.navigate('Profile')}>
                            <View>
                                <Text style={{fontWeight:"bold", fontSize:24, borderWidth:2, width:150, height:50, paddingTop:7, borderRadius:5, textAlign:"center"}}>Cancel</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => submit()}>
                            <View style={{paddingLeft:10}}>
                                <Text style={{fontWeight:"bold", fontSize:24, borderWidth:2, width:150, height:50, paddingTop:7, borderColor:'#FF65C5', borderRadius:5, color:'white', backgroundColor:'#FF65C5', textAlign:"center"}}>Save</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </View>
    );
}

const mapStateToProps = (state) => ({
  profile : state.profileList.profileData,
  loading : state.profileList.isLoading
  })

const mapDispatchToProps = (dispatch) => ({
  editProfileData : (data) => dispatch({type:'EDIT_PROFILE', payload:data}),
})

export default connect(mapStateToProps, mapDispatchToProps)(EditProfileScreen)

const styles = StyleSheet.create ({
    container:{
        flex:1,
        // justifyContent: "center",
        alignItems: 'center',
        backgroundColor: "#F4F4F4",
        marginTop:10,
    },
    logoAvatar:{
        width:150,
        height:150,
        borderRadius:100,
        borderColor:'grey',
        borderWidth:2,
        marginRight:25
    },
    cardBody:{
        flex:2,
        width:400,
        height:800,
        backgroundColor:'white',
        // alignItems:"center",
        borderRadius:25,
        marginTop:10,
        marginBottom:10,
        paddingLeft:20
    },
    cardText:{
        // marginLeft:30,
        marginTop:30,
        marginRight:20,
        marginLeft:10,
        marginBottom:10,
        fontSize:24,
        fontWeight:"bold"
    },
    textInput:{
        borderColor:'gray',
        borderWidth:1,
        borderRadius:10,
        width:350,
        height:50,
        // width:'100%'
        marginLeft:10,
        backgroundColor:'#F4F4F4'
    }
});
