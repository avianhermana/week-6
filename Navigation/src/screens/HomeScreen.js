import React from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Ionicons from 'react-native-vector-icons/Ionicons'
import {connect} from 'react-redux'
import PetCard from '../components/PetCard.component'

function HomeScreen(props){
    return(
        <View>
            <View style={styles.headerContainer}>
                <Image 
                source={require('../assets/tinpet-logo.png')}
                style={styles.logo}/>
                <Ionicons name='notifications' size={wp('6.5%')} color='#9C9C9C' />
            </View>
                <PetCard navigation={props.navigation}/>
        </View>
    )
}

const styles = StyleSheet.create({
    headerContainer: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        height: hp('7.5%'), 
        borderColor: '#E5E5E5', 
        borderWidth: 0.9,
        backgroundColor: '#FFFFFF',
        paddingHorizontal: 15
    },
    logo: {
        marginLeft: wp('31%'),
        width: wp('27%'),
        height: hp('3.7%'),
    },
})

const reduxState = (state) => ({
   
})
const reduxDispatch = () => ({

})
export default connect(reduxState, reduxDispatch)(HomeScreen)