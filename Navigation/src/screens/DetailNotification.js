import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import { ScrollView, TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default function DetailScreen(props){
    return(
        <View style={styles.container}>
            <View style={styles.cardPost}>
                <ScrollView>
                    <View>
                        <View style={styles.profileName}>
                        <Image style={{width:40,height:40, borderRadius:50, position:"relative", marginTop:10, marginLeft:10, borderColor:'white', borderWidth:2 }} source={require("../assets/Avatar.png")}></Image>
                            <Text style={{fontWeight:'bold', marginTop: 20, marginLeft:15}}>MR. JOESTAR</Text>
                        </View>
                    </View>
                    <Image style={styles.cardPostImage} source={require("../assets/kucing.jpeg")}></Image>
                    <View style={{flexDirection:'row', marginTop:5, marginBottom:5, marginLeft:15}}>
                        <Text>1432 Likes</Text>
                        <Text style={{marginLeft:10}}>21 Comments</Text>
                    </View>
                    <View style={{flexDirection:'row', marginLeft:15}}>
                        <Ionicons name="heart" size={30} color="red"/>
                        <Ionicons style={{marginLeft:40}} name="chatbubble-outline" size={30} color="black"/>
                    </View>
                    <View style={{borderTopWidth:1, borderColor:'lightgray'}}></View>
                    
                    <View style={styles.containNotif}>
                        <Image style={styles.photoNotif} source={require('../assets/wongchileek.png')}/>
                            <View style={{flexDirection:'column', width:330}}>
                                <TouchableOpacity onPress={() => props.navigation.navigate('People Profile Screen')}>
                                    <Text style={styles.modalTextName}>Wong Chi Liek</Text>
                                </TouchableOpacity>
                                <Text style={styles.modalTextNotif}> Hahaha Kucingnya lucu , uwwuuuuuuuuuuu bangett, jadi pengen sama pemiliknya hahahaha</Text>
                            </View>
                    </View>

                    <View style={styles.containNotif}>
                        <Image style={styles.photoNotif} source={require('../assets/wongchileek.png')}/>
                            <View style={{flexDirection:'column', width:330}}>
                                <TouchableOpacity onPress={() => props.navigation.navigate('People Profile Screen')}>
                                    <Text style={styles.modalTextName}>Wong Chi Liek</Text>
                                </TouchableOpacity>
                                <Text style={styles.modalTextNotif}> Ayok kita adu kucing nya, berani gak? xD xixixixixixixi</Text>
                            </View>
                    </View>

                </ScrollView>
                <View style={{flexDirection:'row'}}>
                    <View>
                        <Image style={styles.photoNotif1} source={require('../assets/Avatar.png')}/>
                    </View>
                    <View>
                    <TextInput
                        style={{ height: 60, width:280, marginLeft:60, borderColor: 'gray', borderRadius:10, borderWidth:1}}
                        />
                    </View>
                    <View style={{top:15, left:7}}>
                        <TouchableOpacity>
                            <Text>Posting</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems: 'center',
    },
    cardPost:{
        backgroundColor:'white',
        width:400,
        height:700,
        marginTop:20,
        marginBottom:100,
        alignContent:"center"
    },
    cardPostImage:{
        width:400,
        marginTop:1,
        marginBottom:1,
        alignContent:"center"
    },
    profileName:{
        fontWeight:"bold",
        fontSize:24,
        marginBottom:10,
        marginTop:10,
        flexDirection:"row"
    },
    photoNotif:{
        width:50,
        height:50,
        borderRadius:50,
        marginBottom:10
    },
    photoNotif1:{
        borderWidth:1,
        width:50,
        height:50,
        borderRadius:50,
        position:"absolute",
        left:6,
        top:6
    },
    modalTextName: {
        marginLeft:10,
        fontWeight:"bold"
    },
    modalTextNotif: {
        width:327,
        marginBottom: 15,
        marginLeft:7,
        flexWrap:"wrap",
    },
    containNotif:{
        flexDirection:'row',
        marginLeft:10,
        marginBottom:10,
        marginTop:10,
        width:340,
        borderBottomColor:'lightgrey'
    }
})