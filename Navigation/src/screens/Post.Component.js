import React, {useEffect, useState} from 'react';
import {View, Text, Image, StyleSheet,  TouchableOpacity, ActivityIndicator} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {connect} from 'react-redux';
import { ScrollView } from 'react-native-gesture-handler';

function Post(props){
    const navigation = useNavigation();

    const [listPost, setListPost] = useState([]);

    useEffect(() => {
        props.getPostList()
    }, []);

    return(
        <ScrollView>
            {props.loading && <ActivityIndicator size={60} color='#FF65C5' /> }
            {props.post.map((pet, index) => (
                <View key={index} >
                    <View style={styles.cardPost}>
                        <View>
                            <View style={styles.cardProfileName}>
                            <Image style={{width:40,height:40, borderRadius:50, position:"relative", marginTop:10, marginLeft:10, borderColor:'white', borderWidth:2 }} source={{uri:`${pet.userImageUrl}`}}/>
                                <Text style={{fontWeight:'bold', marginTop: 10, marginLeft:15}}>{pet.userFullName}</Text>
                            </View>
                        </View>
                        <TouchableOpacity style={{borderBottomColor:'grey', borderBottomWidth:1}} onPress={() => navigation.navigate("Detail Post", {pet})}>
                            <Image style={styles.cardPostImage} source={{uri:`${pet.imageUrl}`}}/>
                        </TouchableOpacity>
                            <View style={{flexDirection:'row', marginTop:5, marginLeft:25}}>
                                <Text>{pet.likeCount} Suka</Text>
                                <Text style={{marginLeft:'10%'}}>{pet.commentCount} Komentar</Text>
                            </View>
                        
                        <View style={{flexDirection:'row', marginLeft:15,}}>
                            <Ionicons name="heart-outline" size={30} color="black"/>
                            <Ionicons style={{marginLeft:50}} name="chatbubble-outline" size={30} color="black"/>
                        </View>
                    </View>
                </View>
            ))}
        </ScrollView>
    );
}


const styles = StyleSheet.create({
    cardPost:{
        backgroundColor:'white',
        borderRadius:10,
        width:400,
        height:470,
        marginTop:1,
        marginBottom:1,
        alignSelf:"center",
        borderWidth:2,
        marginBottom:10
    },
    cardPostImage:{
        width: 375,
        height: 350,
        alignSelf:"center",
    },
    cardProfileName:{
        fontWeight:"bold",
        alignItems:"center",
        fontSize:24,
        marginBottom:5,
        flexDirection:'row'
    }
});


const reduxState = (state) => ({
    post: state.postList.postData,
    loading: state.postList.isLoading
})

const reduxDispatch = (dispatch) => ({
    getPostList: () => dispatch({type:'GET_PETS'})
})

export default connect(reduxState,reduxDispatch)(Post);