import React, {useState} from 'react'
import {TextInput, View, Text, Image, TouchableOpacity, StyleSheet, ImageBackground} from 'react-native'
import {connect} from 'react-redux'
import {ValidateEmail} from '../common/function/auth'

const LoginScreen = (props) => {
    const [email, setEmail] = useState('wijayadavin@gmail.com');
    const [password, setPassword] = useState('davin12345');

    //username for trial using backend-service local host
    const handleLogin = () => {
        if(!email){
            alert('Email is required')
        }
        else if(!password){
            alert('Password is required')
        }
        else if(!ValidateEmail(email)) {
            alert('Incorrect email format')
        }
        else {
            props.apiLogin({
                email,
                password
            })
        }
    }

    const LoginButton = () => {
        return (
            <TouchableOpacity
            style={styles.buttonWrapper}
            onPress={() => handleLogin()}>
                <Text style={styles.loginTextLabel}>Login</Text>
            </TouchableOpacity>
        )
    }

    const LoadingButton = () => {
        return (
            <TouchableOpacity
            style={styles.buttonWrapperLoading}>
                <Text style={styles.loginTextLabel}>Loading...</Text>
            </TouchableOpacity>
        )
    }

    return(
        <View>
            <ImageBackground
            source={require('../assets/login-background.png')}
            style={styles.backgroundImage}>
                <View style={styles.floatContainer}>
                    <Image 
                    source={require('../assets/tinpet-logo.png')}
                    style={styles.logo}
                    />
                    <View style={{padding: 22}}>
                        <Text style={styles.textLabel}>Email</Text>
                        <TextInput 
                        placeholder='Input your email'
                        placeholderTextColor='#C4C4C4'
                        value={email}
                        style={styles.input}
                        onChangeText={(email) => setEmail(email)}
                        />
                        <Text style={styles.textLabel}>Password</Text>
                        <TextInput 
                        placeholder='Input your password'
                        placeholderTextColor='#C4C4C4'
                        value={password}
                        style={styles.input}
                        onChangeText={(password) => setPassword(password)}
                        secureTextEntry
                        />

                        { props.loading ? <LoadingButton /> : <LoginButton />}

                        <TouchableOpacity 
                        style={styles.signUpTextWrapper}
                        onPress={() => props.navigation.navigate('Register')}>
                            <Text style={styles.signupText}>Don't have an account? Sign up!</Text>
                        </TouchableOpacity>
                    </View>
                </View>
        </ImageBackground>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    backgroundImage: {
        resizeMode: 'cover',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%'
    },
    floatContainer: {
        width: 343,
        height: 500,
        margin: 16,
        marginTop: 10,
        borderColor: '#C4C4C4',
        borderWidth: 1,
        borderRadius: 15,
        backgroundColor: '#ffffff',
    },
    logo: {
        width: 120,
        height: 30,
        alignSelf: 'center',
        marginTop: 39,
        marginBottom: 20
    },
    input: {
        fontSize: 16,
        paddingVertical: 5,
        marginBottom: 19,
        backgroundColor: '#f5f5f5',
        borderRadius: 5,
        borderColor: '#C4C4C4',
        borderWidth: 1,
        paddingLeft: 10,
        paddingVertical: 10,
        color: '#C4C4C4'
    },
    textLabel: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 8,
    },
    loginTextLabel: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#ffffff'
    },
    buttonWrapper: {
        backgroundColor: '#FF65C5',
        alignItems: 'center',
        padding: 16,
        borderRadius: 5,
        marginTop: 37
    },
    buttonWrapperLoading: {
        backgroundColor: '#dc43a6',
        alignItems: 'center',
        padding: 16,
        borderRadius: 5,
        marginTop: 37
    },
    signupText: {
        fontSize: 14, 
        color: '#FF65C5',
        marginTop: 33,
    },
    signUpTextWrapper: {
        flexDirection: 'row-reverse'
    }
})

const reduxState = (state) => ({
    loading: state.login.isLoading
})

const reduxDispatch = (dispatch) => ({
    apiLogin: (data) => dispatch({type: 'LOGIN', payload: data})
})

export default connect(reduxState, reduxDispatch)(LoginScreen);