import React from 'react';
import {View, Text, Image, StyleSheet, ScrollView} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';

export default function DetailPost(props){

    const pet = props.route.params
    console.log(pet);

    return(
        <View style={styles.container}>
            <ScrollView>
            <View style={styles.cardPost}>
                <View>
                    <View style={styles.profileName}>
                    <Image style={{width:40,height:40, borderRadius:50, position:"relative", marginTop:10, marginLeft:10, borderColor:'white', borderWidth:2 }} source={require("../assets/Avatar.png")}></Image>
                        <Text style={{fontWeight:'bold', marginTop: 20, marginLeft:15}}>MR. JOESTAR</Text>
                    </View>
                </View>
                <Image style={styles.cardPostImage} source={require("../assets/kucing.jpeg")}></Image>
                
                <View>
                    <View style={{flexDirection:'row'}}>
                        <View style={{flexDirection:'column', width:'50%', margin:20}}>
                            <Text>Name</Text>
                            <Text style={{fontWeight:'bold', fontSize:18,}}>Mr.Pugsby</Text>
                        </View>
                        <View style={{flexDirection:'column',  width:'50%', margin:20}}>
                            <Text>Gender</Text>
                            <Text style={{fontWeight:'bold', fontSize:18}}>Male</Text>
                        </View>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <View style={{flexDirection:'column', width:'50%', margin:20}}>
                            <Text>Age</Text>
                            <Text style={{fontWeight:'bold', fontSize:18}}>2 years old</Text>
                        </View>
                        <View style={{flexDirection:'column',  width:'50%', margin:20}}>
                            <Text>Category</Text>
                            <Text style={{fontWeight:'bold', fontSize:18}}>Dog</Text>
                        </View>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <View style={{flexDirection:'column', width:'50%', margin:20}}>
                            <Text>Location</Text>
                            <Text style={{fontWeight:'bold', fontSize:18}}>2464 Royal Ln. Mesa, New Jersey 45463</Text>
                        </View>
                        <View style={{flexDirection:'column',  width:'50%', margin:20}}>
                            <Text>Status</Text>
                            <Text style={{fontWeight:'bold', fontSize:18}}>Available</Text>
                        </View>
                    </View>
                </View>
                <View>
                    <View style={{flexDirection:'row', marginTop:5, marginBottom:5, marginLeft:15}}>
                        <Text>1832 Likes</Text>
                        <Text style={{marginLeft:10}}>21 Comments</Text>
                    </View>
                    <View style={{flexDirection:'row', marginLeft:15}}>
                        <Ionicons name="heart-outline" size={30} color="black"/>
                        <Ionicons style={{marginLeft:40}} name="chatbubble-outline" size={30} color="black"/>
                    </View>
                </View>
            </View>
            <View style={styles.containNotif}>
                    <Image style={styles.photoNotif} source={require('../assets/wongchileek.png')}/>
                        <View style={{flexDirection:'column', width:330}}>
                            <TouchableOpacity onPress={() => props.navigation.navigate('People Profile Screen')}>
                                <Text style={styles.modalTextName}>Wong Chi Liek</Text>
                            </TouchableOpacity>
                            <Text style={styles.modalTextNotif}> Ayok kita adu kucing nya, berani gak? xD xixixixixixixi</Text>
                        </View>
                </View>
            </ScrollView>

            <View style={styles.commentBox}>
                <View>
                    <Image style={styles.photoNotif1} source={require('../assets/Avatar.png')}/>
                </View>
                <View>
                    <TextInput
                        style={{ height: 60, width:280, marginLeft:70, borderColor: 'gray', borderRadius:10}}
                        placeholder=" Type your comment"
                    />
                </View>
                <View style={{top:20, left:-20}}>
                    <TouchableOpacity>
                        <Text style={{fontSize: 13.5}}>Posting</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems: 'center',
    },
    cardPost:{
        backgroundColor:'white',
        borderRadius:5,
        width:400,
        height:800,
        marginTop:20,
        // marginBottom:100,
        alignContent:"center"
    },
    cardPostImage:{
        // borderRadius:24,
        width:400,
        marginTop:1,
        marginBottom:1,
        alignContent:"center"
    },
    photoNotif1:{
        borderWidth:1,
        width:50,
        height:50,
        borderRadius:50,
        position:"absolute",
        left:15,
        top:6
    },
    profileName:{
        fontWeight:"bold",
        fontSize:24,
        marginBottom:10,
        marginTop:10,
        flexDirection:"row"
    },
    commentBox:{
        flexDirection:'row',
        backgroundColor:'white',
        width:'100%'
    },
    containNotif:{
        flexDirection:'row',
        marginLeft:10,
        marginBottom:10,
        marginTop:10,
        width:'100%',
        borderBottomColor:'lightgrey',
        backgroundColor:'white',
    },
    photoNotif:{
        width:50,
        height:50,
        borderRadius:50,
        marginBottom:10
    },
    modalTextName: {
        marginLeft:10,
        fontWeight:"bold"
    },
    modalTextNotif: {
        width:327,
        marginBottom: 15,
        marginLeft:7,
        flexWrap:"wrap",
    },
});