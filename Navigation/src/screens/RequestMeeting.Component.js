import React, {useState} from 'react';
import {View, Text, Image, StyleSheet, StatusBar, Dimensions, TextInput, Modal, TouchableHighlight, TouchableOpacity, ScrollView} from 'react-native';
// import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import Ionicons from 'react-native-vector-icons/Ionicons';
import EditProfileScreen from './EditProfileScreen';
import DetailNotification from './DetailNotification';
import DetailPost from './DetailPost';
import {TabView, SceneMap} from 'react-native-tab-view';


export default function SecondPage(){
    return(
        <View>
        <View style={styles.cardRequestMeeting}>
            <View>
                <View style={{flexDirection:'row', width:370}}>
                        <View style={{flexDirection:'row'}}>
                            <TouchableOpacity onPress={() => props.navigation.navigate('People Profile Screen')}>
                                <Image style={styles.photoNotif} source={require('../assets/wongchileek.png')}/>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => props.navigation.navigate('People Profile Screen')}>
                                <Text style={styles.modalTextName}>Wong Chi Liek</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{flexDirection:'row', marginLeft:70}}>
                            <Ionicons style={{ marginTop:7, marginRight:10}} name="close-circle-outline" size={35} color='grey'/>
                            <Ionicons style={{ marginTop:7}} name="checkmark-circle" size={35} color='green'/>
                        </View>
                </View>
                <View>
                    <Text style={styles.modalTextNotif}> Hahaha Kucingnya lucu , uwwuuuuuuuuuuu bangett, jadi pengen sama pemiliknya hahahaha</Text>
                </View>
            </View>
        </View>

        <View style={styles.cardRequestMeeting}>
            <View>
                <View style={{flexDirection:'row', width:370}}>
                        <View style={{flexDirection:'row'}}>
                            <TouchableOpacity onPress={() => props.navigation.navigate('People Profile Screen')}>
                                <Image style={styles.photoNotif} source={require('../assets/wongchileek.png')}/>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => props.navigation.navigate('People Profile Screen')}>
                                <Text style={styles.modalTextName}>Wong Chi Liek</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{flexDirection:'row', marginLeft:70}}>
                            <Ionicons style={{ marginTop:7, marginRight:10}} name="close-circle-outline" size={35} color='grey'/>
                            <Ionicons style={{ marginTop:7}} name="checkmark-circle" size={35} color='green'/>
                        </View>
                </View>
                <View>
                    <Text style={styles.modalTextNotif}> Hahaha Kucingnya lucu , uwwuuuuuuuuuuu bangett, jadi pengen sama pemiliknya hahahaha</Text>
                </View>
            </View>
        </View>
        </View>
    );
}


const styles = StyleSheet.create({
    cardRequestMeeting: {
        width: 400,
        height: 150,
        borderWidth:1,
        borderRadius:15,
        padding:15,
        backgroundColor:'white',
        marginBottom: 15
    },
    photoNotif:{
        width:55,
        height:55,
        borderRadius:50,
        // marginBottom:10
    },
    modalTextName: {
        marginLeft:30,
        paddingTop:5,
        fontSize:18,
        fontWeight:"bold"
    },
    modalTextNotif: {
        width:300,
        marginBottom: 15,
        marginLeft:60,
        marginBottom:30,
        flexWrap:"wrap",
    },
})