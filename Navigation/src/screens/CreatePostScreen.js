import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  ScrollView,
  Image,
  KeyboardAvoidingView,
  TouchableOpacity,
} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import {connect} from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import Ionicons from 'react-native-vector-icons/Ionicons'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

//image
const options = {
  title: 'Select Avatar',
  storageOptions: {
    privateDirectory: true,
    skipBackup: true,
    path: 'images',
  },
};

function CreatePostScreen(props) {
  const [petName, setPetName] = useState();
  const [age, setAge] = useState();
  const [category, setCategory] = useState();
  const [gender, setGender] = useState();
  const [breed, setBreed] = useState();
  const [location, setLocation] = useState();
  const [message, setMessage] = useState();

  const [image, setImage] = useState(
    '../assets/dog01.png',
  );
  const [rawImage, setRawImage] = useState({});

  function PickImage() {
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {
          uri: 'file:///' + response.path,
          type: response.type,
          name: response.fileName,
          data: response.data,
        };
        setRawImage(source);
        setImage(response.uri);
      }
    });
  }

  function uploadImage() {
    const data = new FormData();

    data.append('file', rawImage);

    axios({
      method: 'POST',
      uri: 'http://bda727cc4f04.ngrok.io/upload',
      data: data,
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    })
      .then((res) => {
        console.info('res', res.data);
        setImage(res.data.url);
      })
      .catch((err) => {
        console.error(JSON.stringify(err));
        // console error(JSON stringfy)
      });
  }

  // const [uploadPic, setUploadPic] = useState();

  const checkPost = () => {
    if (age) {
      setMessage('Pet age is required');
    } else if (!category) {
      setMessage('Pet category is required!');
    } else if (!gender) {
      setMessage('Pet gender is required!');
    } else if (!breed) {
      setMessage('Pet breed is required!');
    } else if (!location) {
      setMessage('Pet location is required!');
    } else {
      //process post
      const dataPost = {
        name: petName,
        age: age,
        type: category,
        gender: gender,
        breed: breed,
        city: location,
      };
      props.processPost(dataPost);
    }
  };

  return (
    <View>
      <View style={styles.headerContainer}>
          <Text style={styles.headerTitle}>Create a Post</Text>
          <Ionicons name='notifications' size={25} color='#9C9C9C' />
      </View>
      <ScrollView style={{height: '95%', width: '100%'}}>
        <View>
          <View>
            <Image
              source={require('../assets/background-image-dogs.png')}
              style={styles.petImg}
            />
            <TouchableOpacity 
              style={styles.uploadBtn}
              onPress={() => PickImage()}>
              <Text style={styles.textBtn}>Add Photo</Text>
            </TouchableOpacity>
            <KeyboardAvoidingView style={styles.formContainer}>
              <View>
                <Text style={styles.formHeader}>Pet Name</Text>
                <TextInput
                  style={styles.input1}
                  value={petName}
                  onChangeText={(text) => setPetName(text)}
                />
              </View>
              <View style={{flexDirection: 'row'}}>
                <View>
                  <Text style={styles.formHeader}>Pet Age</Text>
                  <TextInput
                    style={styles.input}
                    value={age}
                    onChangeText={(text) => setAge(age)}
                  />
                </View>
                <View>
                  <Text style={styles.formHeader}>Pet Category</Text>
                  <View style={styles.input}>
                    <Picker
                      selectedValue={category}
                      style={{height: 48, width: 150}}
                      mode="dropdown"
                      onValueChange={(itemValue, itemIndex) =>
                        setCategory(itemValue)
                      }>
                      <Picker.Item label="Dog" value="dog" />
                      <Picker.Item label="Cat" value="cat" />
                    </Picker>
                  </View>
                </View>
              </View>

              <View style={{flexDirection: 'row'}}>
                <View>
                  <Text style={styles.formHeader}>Gender</Text>
                  <View style={styles.input}>
                    <Picker
                      selectedValue={gender}
                      style={{height: 48, width: 150}}
                      mode="dropdown"
                      onValueChange={(itemValue, itemIndex) =>
                        setGender(itemValue)
                      }>
                      <Picker.Item label="Male" value="Male" />
                      <Picker.Item label="Female" value="Female" />
                    </Picker>
                  </View>
                </View>
                <View>
                  <Text style={styles.formHeader}>Breed</Text>
                  <TextInput
                    style={styles.input}
                    value={breed}
                    onChangeText={(text) => setBreed(text)}
                  />
                </View>
              </View>

              <View>
                <Text style={styles.formHeader}>Location</Text>
                <View style={styles.input1}>
                  <Picker
                    selectedValue={location}
                    style={{height: 48, width: 320}}
                    mode="dropdown"
                    onValueChange={(itemValue, itemIndex) =>
                      setLocation(itemValue)
                    }>
                    <Picker.Item label="Jakarta" value="Jakarta" />
                    <Picker.Item label="Bogor" value="Bogor" />
                    <Picker.Item label="Depok" value="Depok" />
                    <Picker.Item label="Tangerang" value="Tangerang" />
                    <Picker.Item label="Bekasi" value="Bekasi" />
                    <Picker.Item label="Bandung" value="Bandung" />
                  </Picker>
                </View>
              </View>

              <Text style={{color: 'red', fontStyle: 'italic'}}>{message}</Text>

              <View style={{flexDirection: 'row'}}>
                <TouchableOpacity style={styles.bottomBtnCancel}>
                  <Text style={styles.textBtn1}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.bottomBtnPost}
                  onPress={() => props.navigation.navigate('Home')}>
                  <Text style={styles.textBtn2}>Post</Text>
                </TouchableOpacity>
              </View>
            </KeyboardAvoidingView>
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

const mapStateToProps = (state) => ({
  //
});

const mapDispatchToProps = (dispatch) => ({
  processPost: (data) => dispatch({type: 'POST', payload: data}),
});

export default connect(mapStateToProps, mapDispatchToProps)(CreatePostScreen);

const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    alignItems: 'center', 
    height: hp('7.5%'), 
    borderColor: '#E5E5E5', 
    borderWidth: 0.9,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 15
},
headerTitle: {
    fontSize: hp('2.5%'), 
    fontWeight: 'bold',
    marginLeft: wp('34%')
},
  header: {
    width: '100%',
    height: 54,
    marginTop: '6%',
    borderColor: '#E5E5E5',
    borderWidth: 1,
  },
  textHeader: {
    width: '100%',
    justifyContent: 'center',
    fontFamily: 'montserrat',
    fontSize: 23,
    fontWeight: 'bold',
    marginVertical: '2.5%',
    marginHorizontal: '33%',
  },
  container: {
    marginTop: 16,
  },
  petImg: {
    width: 167,
    height: 115,
    marginHorizontal: '30.5%',
    marginTop: 25,
  },
  uploadBtn: {
    width: 167,
    height: 55,
    backgroundColor: '#3E4C6F',
    borderRadius: 5,
    marginTop: 20,
    marginHorizontal: '30.5%',
  },
  textBtn: {
    fontFamily: 'montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#fff',
    height: 22,
    width: 130,
    marginTop: 14,
    marginHorizontal: '25%',
  },
  formContainer: {
    alignItems: 'center',
    paddingBottom: 100,
  },
  formHeader: {
    marginTop: 16,
    fontWeight: 'bold',
    fontSize: 18,
    marginLeft: 14,
    paddingBottom: 8,
  },
  input1: {
    width: 343,
    height: 50,
    backgroundColor: '#F4F4F4',
    borderRadius: 5,
    marginLeft: '1%',
    borderColor: '#C4C4C4',
    borderWidth: 1,
    fontSize: 18,
    paddingLeft: 10,
  },
  input: {
    width: 164,
    height: 50,
    marginLeft: '3%',
    backgroundColor: '#F4F4F4',
    borderRadius: 5,
    borderColor: '#C4C4C4',
    borderWidth: 1,
    paddingLeft: 10,
  },
  bottomBtnCancel: {
    borderColor: '#000000',
    borderWidth: 2,
    marginTop: 29,
    width: 167,
    height: 55,
    borderRadius: 5,
  },
  bottomBtnPost: {
    backgroundColor: '#FF65C5',
    marginTop: 29,
    width: 167,
    height: 55,
    borderRadius: 5,
    marginLeft: 8,
  },
  textBtn1: {
    width: 168,
    color: '#000000',
    fontSize: 18,
    fontWeight: 'bold',
    marginHorizontal: '34.5%',
    marginVertical: '7%',
  },
  textBtn2: {
    fontFamily: 'montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#fff',
    height: 22,
    width: 130,
    marginHorizontal: '39%',
    marginVertical: '9%',
  },
  dropdown: {
    width: 164,
    height: 50,
    marginLeft: '3%',
    backgroundColor: '#F4F4F4',
    borderRadius: 5,
    borderColor: '#C4C4C4',
    borderWidth: 1,
  },
});
