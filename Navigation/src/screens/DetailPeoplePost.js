import React from 'react';
import {View, Text, Image, StyleSheet, ScrollView} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';

export default function DetailPeoplePost(props){
    return(
        <View style={styles.container}>
            <View style={styles.cardPost}>
                <View>
                    <View style={styles.profileName}>
                    <Image style={{width:40,height:40, borderRadius:50, position:"relative", marginTop:10, marginLeft:10, borderColor:'white', borderWidth:2 }} source={require("../assets/wongchileek.png")}></Image>
                        <Text style={{fontWeight:'bold', marginTop: 20, marginLeft:15}}>MR. JOESTAR</Text>
                    </View>
                </View>
                <Image style={styles.cardPostImage} source={require("../assets/kucing.jpeg")}></Image>
                <View style={{flexDirection:'row', marginTop:5, marginBottom:5, marginLeft:15}}>
                    <Text>1432 Likes</Text>
                    <Text style={{marginLeft:10}}>21 Comments</Text>
                </View>
                <View style={{flexDirection:'row', marginLeft:15}}>
                    <Ionicons name="heart-outline" size={30} color="black"/>
                    <Ionicons style={{marginLeft:40}} name="chatbubble-outline" size={30} color="black"/>
                </View>
            </View>
            <ScrollView>
                <View style={styles.commentBox}>
                    <View>
                        <Image style={styles.photoNotif1} source={require('../assets/Avatar.png')}/>
                    </View>
                    <View>
                    <TextInput
                        style={{ height: 60, width:280, marginLeft:60, borderColor: 'gray', borderRadius:10, borderWidth:1}}
                        />
                    </View>
                    <View style={{top:15, left:7}}>
                        <TouchableOpacity>
                            <Text>Posting</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems: 'center',
    },
    cardPost:{
        backgroundColor:'white',
        borderRadius:5,
        width:400,
        height:500,
        marginTop:20,
        marginBottom:100,
        alignContent:"center"
    },
    cardPostImage:{
        width:400,
        marginTop:1,
        marginBottom:1,
        alignContent:"center"
    },
    photoNotif1:{
        borderWidth:1,
        width:50,
        height:50,
        borderRadius:50,
        position:"absolute",
        left:6,
        top:6
    },
    profileName:{
        fontWeight:"bold",
        fontSize:24,
        marginBottom:10,
        marginTop:10,
        flexDirection:"row"
    },
    commentBox:{
        flexDirection:"row",
        width:400,
        height:65,
        backgroundColor:'white'
    }
});