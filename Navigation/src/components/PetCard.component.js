import axios from 'axios';
import React, {useState, useEffect} from 'react';
import {StyleSheet, View, Image, Text, TouchableOpacity, ScrollView, ActivityIndicator} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {connect} from 'react-redux'

function PetCard(props) {
  const [listPet, setListPet] = useState([]);

  useEffect(() => {
    props.getPetList()
  }, []);

  return (
    <ScrollView style={{marginTop: 10, marginBottom: hp('8%')}}>
      {/* looping */}
      {props.loading && <ActivityIndicator size={60} color='#FF65C5' /> }
      {props.pets.map((pet, index) => (
        <View key={index}>
        <TouchableOpacity activeOpacity={0.5} onPress={() => props.navigation.navigate('PetDetail')}>
          <View style={styles.container}>
            <View style={styles.containerItem}>
              <Image
                style={{width: '100%', height: '100%', borderRadius: 15}}
                source={{
                  uri: `https://${pet.imageUrl}`
                }}
              />
              <Image
                style={styles.profileImage}
                source={{
                  uri: `https://${pet.userImageUrl}`
                }}
              />
            </View>
            <TouchableOpacity
              style={styles.buttonWrapper}
              onPress={() => props.navigation.navigate('Request')}>
              <Text style={styles.TextLabel}>Request a Meeting</Text>
            </TouchableOpacity>

            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  color: '#7B7B7B',
                  fontWeight: 'bold',
                  fontSize: 14,
                  marginTop: 8,
                  marginLeft: '5%',
                }}>
                {pet.likeCount} Likes
              </Text>
              <Text
                style={{
                  color: '#7B7B7B',
                  fontWeight: 'bold',
                  fontSize: 14,
                  marginTop: 8,
                  marginLeft: 8,
                }}>
                {pet.commentCount} Comments
              </Text>
            </View>

            <View style={{flexDirection: 'row', marginHorizontal: '5%'}}>
              <TouchableOpacity>
                <Ionicons
                  style={styles.smallIcon}
                  name="heart-outline"
                  size={35}
                  onClick={() => setCountLike(countLike + 1)}
                />
              </TouchableOpacity>

              <TouchableOpacity>
                <Ionicons
                  style={styles.smallIcon}
                  name="chatbubble-outline"
                  onClick={() => setCountComment(countComment + 1)}
                  size={35}
                />
              </TouchableOpacity>
            </View>
          </View>
          </TouchableOpacity>
        </View>
      ))}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '90%',
    height: 401,
    backgroundColor: '#ffff',
    borderRadius: 15,
    marginLeft: '5%',
    marginRight: '5%',
    borderColor: '#E5E5E5',
    borderWidth: 1,
    marginBottom: 20,
  },
  containerItem: {
    borderRadius: 15,
    height: 259,
    justifyContent: 'center',
  },

  profileImage: {
    borderRadius: 20,
    height: 40,
    width: 40,
    position: 'absolute',
    left: '3.21%',
    top: '4%',
  },
  TextLabel: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'white',
  },
  buttonWrapper: {
    backgroundColor: '#FF65C5',
    width: '90%',
    height: 55,
    alignItems: 'center',
    padding: 16,
    borderRadius: 5,
    marginTop: 8,
    marginHorizontal: '5%',
  },
  smallIcon: {
    marginTop: 8,
    marginLeft: '5%',
    marginRight: '5%',
  },
});

const reduxState = (state) => ({
    pets: state.petList.petData,
    loading: state.petList.isLoading
})
const reduxDispatch = (dispatch) => ({
    getPetList: () => dispatch({type: 'GET_PETS'})
})
export default connect(reduxState, reduxDispatch)(PetCard);