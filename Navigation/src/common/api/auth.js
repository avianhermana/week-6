import axios from 'axios';

export function getPostApi(id, headers){
    return axios({
        method: 'GET',
        url: "https://api.tinpet.my.id/pet?userId="+id,
        headers
        })
}

export function getProfile(headers){
    return axios({
        method: "GET",
        url: "https://api.tinpet.my.id/profile",
        headers
    })
}

export function apiEditProfile(headers, payload){
    return axios({
        method: "PATCH",
        url: "https://api.tinpet.my.id/profile",
        payload,
        headers
    })
}
