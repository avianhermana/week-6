import React from 'react';
import {StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native'

//screens
import LoginScreen from './src/screens/LoginScreen'
import RegisterScreen from './src/screens/RegisterScreen'
import RequestScreen from './src/screens/RequestScreen'
import AppStack from './src/navigator/AppStack'
import MainNavigator from './src/navigator/MainNavigator'

//redux
import {Provider} from 'react-redux'
import store from './src/store'

const App = () => {
  return (
    <Provider store={store}>
      <StatusBar barStyle="dark-content" />
      <NavigationContainer>
        <AppStack />
      </NavigationContainer>
    </Provider>
  );
};

export default App;
